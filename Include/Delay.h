#ifndef __DELAY_H__
#define __DELAY_H__

#include "stdint.h"

void delay_us(uint16_t us);

#endif
