#ifndef __LTC6804_H__
#define __LTC6804_H__

/*---General Dependencies----------------------------------------------------------------*/
#include "stdint.h"
#include "stm32f4xx.h"
#include "Typedefs.h"
#include "Defines.h"
/*---Timing Dependencies----------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "Delay.h"
/*---Custom Dependencies----------------------------------------------------------------*/
#include "LTC6804_Registers.h"
#include "LTC6804_Config.h"
#include "SPI.h"

/*---Public Driver properties----------------------------------------------------------------*/

/*Driver settings*/
#define LTC6804_MAX_PACKS	8
#define LTC6804_NUM_CELLV	96
#define LTC6804_NUM_GPIO	5
#define LTC6804_NUM_TEMP	96
#define LTC6804_ADC_CLEAR	0xFFFF

#define LTC6804_COMMAND_SIZE					2
#define LTC6804_REGISTER_GROUP_SIZE		6
#define LTC6804_PEC_SIZE 							2
#define LTC6804_RDGROUP_SIZE 					4



/*---Public Function prototypes----------------------------------------------------------------*/
void LTC6804_Init(void);
void LTC6804_GPIO_Init(uint8_t pack_num);
void Global_Wakeup(void);
void LTC6804_Refon_Set(uint8_t pack_num, BOOLEAN refon);
void LTC6804_UVOV_Thresholds_Set(uint8_t pack_num, uint16_t vuv_value, uint16_t vov_value);
void LTC6804_Discarge_Set(uint8_t pack_num, uint16_t discharge_bitmap, uint16_t timeout_value);
void LTC6804_GPIO_Clear(uint8_t pack_num);
void LTC6804_GPIO_Start(uint8_t pack_num);
BOOLEAN LTC6804_GPIO_Read(uint8_t pack_num, uint16_t *adc_value_ptr);
void LTC6804_Voltage_Clear(uint8_t pack_num);
void LTC6804_Voltage_Start(uint8_t pack_num, BOOLEAN discharge_permitted);
BOOLEAN LTC6804_Voltage_Read(uint8_t pack_num,uint16_t *adc_value_ptr);
void LTC6804_I2C_Write(uint8_t pack_num, uint8_t sensor, BOOLEAN enable);

#endif
