#ifndef __LTC6804_CONFIG_H__
#define __LTC6804_CONFIG_H__

/*---General Dependencies----------------------------------------------------------------*/
#include "System.h"

#define LTC6804_CONFIG_NUM_BOARDS SYSTEM_MAX_BOARDS


#define LTC6804_GET_BOARD_ADDRESS(pack_num) (\
	System_Address_Table[pack_num] | LTC6804_COMMANDCODE_BASE_ADRESSBIT)


#endif
