#ifndef __TEMPERATURE_H__
#define __TEMPERATURE_H__

/*---General Dependencies----------------------------------------------------------------*/
#include "stdint.h"
#include "stm32f4xx.h"
#include "Typedefs.h"
#include "Defines.h"
#include "System.h"
#include "Global.h"
/*---Timing Dependencies----------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "Delay.h"
/*---Custom Dependencies----------------------------------------------------------------*/
#include "LTC6804.h"
#include "SPI.h"

/*---Public Driver properties----------------------------------------------------------------*/


/*---Public function prototypes----------------------------------------------------------------*/
void Temperature_Init(void);
BOOLEAN Temperature_Wakeup_Init(void);
void Temperature_Monitor(void);


#endif
