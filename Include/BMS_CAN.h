#ifndef __BMS_CAN_H__
#define __BMS_CAN_H__

/* Dependencies -------------------------------------------- */
#include "stdint.h"
#include "stm32f4xx_can.h"
#include "misc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "ioMapping.h"
#include "BMS_CAN_ID.h"
#include "Typedefs.h"
#include "System.h"

/* MACROS ---------------------------------------------------*/
#define BYTE2HALFWORD(MSB,LSB) ( (MSB << 8) | (LSB & 0xff ))
#define MASK_LSB(data) (uint8_t)(data & 0xFF)
#define MASK_MSB(data) (uint8_t)(data >> 8)
/* Check if the can recieve message intterupt occured */
#define IS_MSG_PENDING_FIFO0(CANx) ((CANx->RF0R & CAN_RF0R_FMP0) != (uint32_t)0)
#define IS_MSG_PENDING_FIFO1(CANx) ((CANx->RF1R & CAN_RF1R_FMP1) != (uint32_t)0)

/*Constants -------------------------------------------------*/

typedef union CanFrame
{
	struct
	{		
		uint32_t ID:32;
		uint8_t Name[8];		
		uint8_t Write:1;
	}CanStructtekst;

	struct
	{	
		uint32_t ID:32;
		uint8_t Error:1;
		uint8_t Dummy1:7;
		
		uint8_t Dummy2:8;
		uint8_t Dummy3:8;
		
		uint8_t Fan:1;
		uint8_t IMD_err:1;
		uint8_t LL_lim:1;
		uint8_t L_lim:1;
		uint8_t H_lim:1;
		uint8_t HH_lim:1;
		uint8_t SPI_Temp_err:1;
		uint8_t SPI_Volt_err:1;
		uint8_t Dummy4:2;
		
		uint8_t Error_code:8;
		
		uint8_t Error_Value:8;
		
		uint8_t HighTemp:1;
		uint8_t LowTemp:1;
		uint8_t HighVolt:1;
		uint8_t LowVolt:1;
		uint8_t Dummy5:4;
		
		uint8_t Dummy6:8;
		
		uint8_t Write:1;
	}CanStructState;
	
	struct
	{	
		uint32_t ID:32;
		uint8_t d0:8;
		uint8_t d1:8;
		uint8_t d2:8;
		uint8_t d3:8;
		uint8_t d4:8;
		uint8_t d5:8;
		uint8_t d6:8;
		uint8_t d7:8;	

		uint8_t Write:1;
	}CanStructSingle;
	
	struct
	{	
		uint32_t ID:32;
		uint16_t d0:16;
		uint16_t d2:16;
		uint16_t d4:16;
		uint16_t d6:16;
		uint8_t Write:1;
		
	
	}CanStructDouble;	
		struct
	{	
		uint32_t ID:32;
		uint32_t d0:32;
		uint32_t d4:32;
		uint8_t Write:1;
		
	
	}CanStructLong;
		struct
	{	
		uint32_t ID:32;
		uint16_t d0:16;
		uint8_t d2:8;
		uint8_t d3:8;
		uint8_t d4:8;
		uint8_t d5:8;
		uint8_t d6:8;
		uint8_t d7:8;
		uint8_t Write:1;
	}CanStructCustomFrame;

	struct
	{
		uint32_t ID:32;
		uint8_t Data[8];
		uint8_t Write:1;
	}CanStructArrayFrame;	

	struct
	{
		uint32_t ID:32;
		uint8_t Data[8];
		uint8_t Write:1;
	}FullFrame;
	
}BMS_CAN_frame_t;


/*Public functions ------------------------------------------*/
void BSP_CAN_init(void);
void BSP_CAN2_init(void);
void CANBuffer_Init(BMS_CAN_frame_t *buffer);
void CANBuffer_copy(BMS_CAN_frame_t *buffer, uint16_t *Data, uint8_t cnt, uint8_t base, uint16_t div);
#endif
