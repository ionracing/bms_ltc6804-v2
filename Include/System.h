#ifndef __SYSTEM_H__
#define __SYSTEM_H__

/*---General Dependencies----------------------------------------------------------------*/
#include "stdint.h"
#include "Typedefs.h"

/*---Public Driver properties----------------------------------------------------------------*/
#define SYSTEM_MAX_BOARDS 1
#define SYSTEM_NUM_CELLS 12
#define SYSTEM_NUM_TEMP 12

#define SYSTEM_NAME "Munnin"
#define	SYSTEM_REV	"CANV1.00"

#define CAN_BASE_ID				200
#define CAN_REG_SIZE			15

#define TEST_BUFFER_SIZE 3

#define CAN_BUFFER_SIZE 	(9 + TEST_BUFFER_SIZE * SYSTEM_MAX_BOARDS)

#define CELL_MAXVOLTAGE 40000
#define CELL_HIGHVOLTAGE 39000
#define CELL_MINVOLTAGE 33000
#define CELL_MAXTEMPERATURE 105


/*---Driver typedefs----------------------------------------------------------------*/
typedef enum {
    SYSTEM_STATE_OFF,                       // 
    SYSTEM_STATE_STM_BOARD_INIT,            // 
    SYSTEM_STATE_INIT,                      // 
    SYSTEM_STATE_AWAKE,                     // 
    SYSTEM_STATE_SLEEP,                     // 
    SYSTEM_NUM_STATES
} SYSTEM_STATE_TYPE;

/*---external parameters----------------------------------------------------------------*/
extern uint8_t System_Address_Table[SYSTEM_MAX_BOARDS];
extern uint8_t System_Num_Boards;
extern SYSTEM_STATE_TYPE System_State;



/*---Public function prototypes----------------------------------------------------------------*/
BOOLEAN System_Powered_Up(void);

#endif
