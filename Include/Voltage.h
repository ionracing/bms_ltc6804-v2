#ifndef __VOLTAGE_H__
#define __VOLTAGE_H__

/*---General Dependencies----------------------------------------------------------------*/
#include "Typedefs.h"
#include "System.h"

/*---Custom Dependencies----------------------------------------------------------------*/
#include "LTC6804.h"
#include "Global.h"

/*---Public Function prototypes----------------------------------------------------------------*/
void Voltage_Init(void);
void Voltage_Monitor(void);



#endif
