#ifndef __BATTERYMANAGER_H__
#define __BATTERYMANAGER_H__

/*Dependencies------------------------------------------------------------------------------*/
#include "stdint.h"
#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "Defines.h"
#include "LTC6804.h"
#include "Voltage.h"
#include "Temperature.h"
#include "Current.h"

#include "BMS_PWM.h"
#include "Extern_Global_Values.h"

/*Define fan output settings*/
#define FANPORT	GPIOC
#define FAN1PIN 6
#define FAN2PIN	7
#define FAN3PIN	8
#define FAN4PIN	9
#define HIGHTEMP 30000
#define LOWTEMP 25000

/*Function declarations*/
void Batterymanager_init(void);
void Battery_Monitor_Task(void);
void Voltage_Monitor_Task(void);
void Temperature_Monitor_Task(void);
void Current_Monitor_Task(void);
void Balancer_Control_Task(void);


#endif
