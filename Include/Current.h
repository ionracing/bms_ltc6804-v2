#ifndef __CURRENT_H__
#define __CURRENT_H__

/*---General Dependencies----------------------------------------------------------------*/
#include "stdint.h"
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_adc.h"
#include "Typedefs.h"
#include "Defines.h"
#include "System.h"
#include "Global.h"

void current_Init(void);
uint16_t current_read(void);



#endif
