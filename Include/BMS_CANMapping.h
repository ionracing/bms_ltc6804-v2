#ifndef __BMS_CANMAPPING_H__
#define __BMS_CANMAPPING_H__
* Battery ----------------------------- */
#define ID_PRECHARGE 1303 /* Precharge done */
#define ID_BMS_VOLTAGE 0x623 /* Contains pack voltage */
#define ID_BMS_CURRENT 841 /*Battery current from SM in battery */

/* Pedal modul --------------------------*/
#define ID_BWT 93 /* Brake while throttling state d0 = 1/0 (error/no-error) */
#define ID_BRAKE 1202 /* Toggles brakelight D0 = 1 / 0  (on/off)*/
#define ID_THROTTLE 0x00
/* Sensor out of range should not set the car in critical error 
   Need to reprogram the pedal module to fix this.
*/
#define ID_PEDALS_IMP 91 /* Sensor out of range -> critical error ... */
/* Display ------------------------------*/
#define ID_STOP 0x0E
#define ID_START 0xAA

/** 
For updating parameters on the microcontroller.
enum regParam is the d0 of the frame.
D1(LSB) and D2(MSB) new data 
*
*/
#define ID_PARAM_UPDATE 20 
enum regParam
{
    regKp, /* Gain constant for regulator */
    regKi, /* Integral gain constant for regulator */
    regKd, /* Derivative gain constant for regulator */
    regPlim, /* Maximum power set point for regulator */
    regLoggUpdateRate, /* Update VSI logging. d1 contains sensor number
                            and d2 contains update rate in ms (8bit)
                        */
};

/*
* regLoggUpdateRate d0 value corresponds to the folowing sensors in order.
* ie d2 = 0 -> REG_VQ
*    d2 = 1 -> REG_VD
*/
#define REG_VQ 0x29 /* Vq voltage */
#define REG_VD 0x2a /* Vd voltage */
#define REG_IQ_ACT 0x27 /* Iq current */
#define REG_ID_ACT 0x28 /* Id current */
#define REG_POWER 0xf6 /* Motor power */
#define REG_SPEED 0x30 /* Motor speed */
#define REG_SPEED_FILT 0xa8 /* Filtered motor speed */
#define REG_T_MOTOR 0x49 /* The temperature of the motor windings */
#define REG_T_IGBT 0x4A /* The transistors temperature */
#define REG_T_AIR 0x4B /* The air temperature of the inverter */

/* Transmit IDs ---------------------------------------------------------------------*/
/* Inverter ---------------------------- */
/* See E-DS-NDrive.pdf and E-DS-CAN for more information */
#define ID_VSI_TX 0x210
#define ID_VSI_RX 0x190
#define REG_REQUEST_DATA 0x3d /* Used for requesting data from the motorcontroller */ 
#define REG_REQUEST_TORQUE 0x90 /* Used for setting a torque to the motorcontroller */

/* Error messages ----------------------*/
/*
* ID    : ID_ERROR
* D0    : D0_error
* D1    : error state true/false (1/2)
*/
#define ERROR_ID 10
#define ERROR_TYPE_BWT 1 /* Braking while throttling */
#define ERROR_TYPE_PI 2 /* Pedal implausability - currently not used, need to update the pedal module*/
#define ERROR_TYPE_CRITICAL 3 /* Criticall error -> car stops permanently */
#define ERROR_TRUE 1
#define ERROR_FALSE 2


#endif
