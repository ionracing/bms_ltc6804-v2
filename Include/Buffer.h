#ifndef __BUFFER_H__
#define __BUFFER_H__

/*---General Dependencies----------------------------------------------------------------*/
#include "stdint.h"
#include "Typedefs.h"
#include "eError.h"
#include "stm32f4xx_can.h"
#include "System.h"

/*---Public Driver properties----------------------------------------------------------------*/


/*---Driver typedefs----------------------------------------------------------------*/
typedef CanRxMsg tElement;

/*Buffer for storing data for canbus messages.*/
typedef struct
{
	uint32_t ID;
	uint8_t Data[8];
	uint8_t read;
	uint8_t write;
}CanbusWriteBuffer_t;

typedef struct 
{
    tElement *buf;  /* Block of memory*/
    uint16_t size;  /* must be a power of two */
    uint16_t read;  /* Holds current read position: 0 to (size-1)  */
    uint16_t write; /* Hods current write position: 0 to (size-1) */
}circBuffer_t;
/*---external parameters----------------------------------------------------------------*/

/*---Public function prototypes----------------------------------------------------------------*/
void buffer_init(circBuffer_t* cb, tElement* buffer, uint16_t length);
void bufferData_Init(CanbusWriteBuffer_t *cwb, uint16_t length);
enum eError buffer_write(circBuffer_t *cb, tElement data);
enum eError buffer_read(circBuffer_t *cb, tElement *data);

#endif
