#ifndef __LTC6804_REGISTERS_H__
#define __LTC6804_REGISTERS_H__

/*---General Dependencies----------------------------------------------------------------*/
#include "stdint.h"
#include "stm32f4xx.h"
#include "Typedefs.h"
#include "Defines.h"


/*---Public Driver properties----------------------------------------------------------------*/

/*LTC6804 Adress Command format, defined by table 33 from datasheet*/
#define LTC6804_COMMANDCODE_BASE_ADRESSBIT	0x10			//Enable bit for sending to adressable LTC6804-2 slaves
#define LTC6804_COMMANDCODE_ADDRESS(command, adress)	((command & 0x07FF) | ((uint16_t) (adress & 0x1F) << 11))


/*LTC6804 Base Commands defined by table 34 from datasheet*/
#define LTC6804_COMMANDCODE_BASE_WRCFG			0X001			//Write Configuration Register Group
#define LTC6804_COMMANDCODE_BASE_RDCFG			0X002			//Read Configuration Register Group
#define LTC6804_COMMANDCODE_BASE_RDCVA			0X004			//Read Cell Voltage Register Group A
#define LTC6804_COMMANDCODE_BASE_RDCVB			0X006			//Read Cell Voltage Register Group B
#define LTC6804_COMMANDCODE_BASE_RDCVC			0X008			//Read Cell Voltage Register Group C
#define LTC6804_COMMANDCODE_BASE_RDCVD			0X00A			//Read Cell Voltage Register Group D
#define LTC6804_COMMANDCODE_BASE_RDAUXA			0X00C			//Read Auxiliary Register Group A
#define LTC6804_COMMANDCODE_BASE_RDAUXB			0X00E			//Read Auxiliary Register Group B
#define LTC6804_COMMANDCODE_BASE_RDSTATA		0X010			//Read Status Register Group A
#define LTC6804_COMMANDCODE_BASE_RDSTATB		0X012			//Read Status Register Group B
#define LTC6804_COMMANDCODE_BASE_ADCV				0X260			//Start Cell Voltage ADC Conversion and Poll Status
#define LTC6804_COMMANDCODE_BASE_ADOW				0X228			//Start Open Wire ADC Conversion and Poll Status
#define LTC6804_COMMANDCODE_BASE_CVST				0X207			//Start Self-Test Cell Voltage Conversion and Poll Status
#define LTC6804_COMMANDCODE_BASE_ADAX				0X460			//Start GPIOs ADC Conversion and Poll Status
#define LTC6804_COMMANDCODE_BASE_AXST				0X407			//Start Self-Test GPIOs Conversion and Poll Status
#define LTC6804_COMMANDCODE_BASE_ADSTAT			0X468			//Start Status group ADC Conversion and Poll Status
#define LTC6804_COMMANDCODE_BASE_STATST			0X40F			//Start Self-Test Status group Conversion and Poll Status
#define LTC6804_COMMANDCODE_BASE_ADCVAX			0X46F			//Start Combined Cell Voltage and GPIO1, GPIO2 Conversion and Poll Status
#define LTC6804_COMMANDCODE_BASE_CLRCELL		0X711			//Clear Cell Voltage Register Group
#define LTC6804_COMMANDCODE_BASE_CLRAUX			0X712			//Clear Auxiliary Register Group
#define LTC6804_COMMANDCODE_BASE_CLRSTAT		0X713			//Clear Status Register Group
#define LTC6804_COMMANDCODE_BASE_PLADC			0X714			//Poll ADC Conversion Status
#define LTC6804_COMMANDCODE_BASE_DIAGN			0X715			//Diagnose MUX and Poll Status
#define LTC6804_COMMANDCODE_BASE_WRCOMM			0X721			//Write COMM Register Group
#define LTC6804_COMMANDCODE_BASE_RDCOMM			0X722			//Read COMM Register Group
#define LTC6804_COMMANDCODE_BASE_STCOMM			0X723			//Start I2C/SPI Communication

/*LTC6804 Commands defined by table 34 from datasheet*/
#define LTC6804_COMMANDCODE_WRCFG(adress)										\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_WRCFG, adress)	//Write Configuration Register Group
#define LTC6804_COMMANDCODE_RDCFG(adress)										\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_RDCFG, adress)	//Read Configuration Register Group
#define LTC6804_COMMANDCODE_RDCVA(adress)										\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_RDCVA, adress)	//Read Cell Voltage Register Group A
#define LTC6804_COMMANDCODE_RDCVB(adress)										\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_RDCVB, adress)	//Read Cell Voltage Register Group B
#define LTC6804_COMMANDCODE_RDCVC(adress)										\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_RDCVC, adress)	//Read Cell Voltage Register Group C
#define LTC6804_COMMANDCODE_RDCVD(adress)										\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_RDCVD, adress)	//Read Cell Voltage Register Group D
#define LTC6804_COMMANDCODE_RDAUXA(adress)									\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_RDAUXA, adress)	//Read Auxiliary Register Group A
#define LTC6804_COMMANDCODE_RDAUXB(adress)									\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_RDAUXB, adress)	//Read Auxiliary Register Group B
#define LTC6804_COMMANDCODE_RDSTATA(adress)									\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_RDSTATA, adress)	//Read Status Register Group A
#define LTC6804_COMMANDCODE_RDSTATB(adress)									\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_RDSTATB, adress)	//Read Status Register Group B
#define LTC6804_COMMANDCODE_ADCV(adress, md, dcp, ch)				\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_ADCV, adress)	+ ((uint16_t)(md & 0X03)<< 7) + ((uint16_t)(dcp & 0X01)<< 4) 	+ ((uint16_t)(ch & 0X07))	//Start Cell Voltage ADC Conversion and Poll Status
#define LTC6804_COMMANDCODE_ADOW(adress, md, pup, dcp, ch)	\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_ADOW, adress)	+ ((uint16_t)(md & 0X03)<< 7) + ((uint16_t)(pup & 0X01)<< 6)	+ ((uint16_t)(dcp & 0X01)<< 4)	+ ((uint16_t)(ch & 0X07))//Start Open Wire ADC Conversion and Poll Status
#define LTC6804_COMMANDCODE_CVST(adress, md, st)						\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_CVST, adress)	+ ((uint16_t)(md & 0X03)<< 7) + ((uint16_t)(st & 0X03)<< 5)//Start Self-Test Cell Voltage Conversion and Poll Status
#define LTC6804_COMMANDCODE_ADAX(adress, md, chg)						\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_ADAX, adress)	+ ((uint16_t)(md & 0X03)<< 7) + ((uint16_t)(chg & 0X07))//Start GPIOs ADC Conversion and Poll Status
#define LTC6804_COMMANDCODE_AXST(adress, md, st)						\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_AXST, adress)	+ ((uint16_t)(md & 0X03)<< 7) + ((uint16_t)(st & 0X03)<< 5)//Start Self-Test GPIOs Conversion and Poll Status
#define LTC6804_COMMANDCODE_ADSTAT(adress, md, chst)				\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_ADSTAT, adress)+ ((uint16_t)(md & 0X03)<< 7) + ((uint16_t)(chst & 0X07))	//Start Status group ADC Conversion and Poll Status
#define LTC6804_COMMANDCODE_STATST(adress, md, st)					\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_STATST, adress)+ ((uint16_t)(md & 0X03)<< 7) + ((uint16_t)(st & 0X03)<< 5)	//Start Self-Test Status group Conversion and Poll Status
#define LTC6804_COMMANDCODE_ADCVAX(adress, md, dcp)					\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_ADCVAX, adress)+ ((uint16_t)(md & 0X03)<< 7) + ((uint16_t)(dcp & 0X01)<< 4)	//Start Combined Cell Voltage and GPIO1, GPIO2 Conversion and Poll Status
#define LTC6804_COMMANDCODE_CLRCELL(adress)									\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_CLRCELL, adress)	//Clear Cell Voltage Register Group
#define LTC6804_COMMANDCODE_CLRAUX(adress)									\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_CLRAUX, adress)	//Clear Auxiliary Register Group
#define LTC6804_COMMANDCODE_CLRSTAT(adress)									\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_CLRSTAT, adress)	//Clear Status Register Group
#define LTC6804_COMMANDCODE_PLADC(adress)										\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_PLADC, adress)	//Poll ADC Conversion Status
#define LTC6804_COMMANDCODE_DIAGN(adress)										\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_DIAGN, adress)	//Diagnose MUX and Poll Status
#define LTC6804_COMMANDCODE_WRCOMM(adress)									\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_WRCOMM, adress)	//Write COMM Register Group
#define LTC6804_COMMANDCODE_RDCOMM(adress)									\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_RDCOMM, adress)	//Read COMM Register Group
#define LTC6804_COMMANDCODE_STCOMM(adress)									\
				LTC6804_COMMANDCODE_ADDRESS(LTC6804_COMMANDCODE_BASE_STCOMM, adress)	//Start I2C/SPI Communication

#define LTC6804_MEM_GPIOx(gpiox) ((uint8_t)(gpiox & 0x1F)<< 3)
#define LTC6804_MEM_REFON(refon) ((uint8_t)(refon & 0x01)<< 2)
#define LTC6804_MEM_SWTRD(swtrd) ((uint8_t)(swtrd & 0x01)<< 1)
#define LTC6804_MEM_ADCOPT(adcopt) ((uint8_t)(adcopt & 0x01))
#define LTC6804_MEM_VUV(vuv) ((uint16_t)(vuv & 0x0FFF))
#define LTC6804_MEM_VOV(vov) ((uint16_t)(vov & 0x0FFF)<< 4)
#define LTC6804_MEM_DCC(dcc) ((uint16_t)(dcc & 0x0FFF))
#define LTC6804_MEM_DCTO(dcto) ((uint8_t)(dcto & 0x0F)<< 4)

/*LTC6804 Command Bit, defined by Table 35 from datasheet*/
#define LTC6804_ADCOPT_0	0x0			//ADC Mode High
#define LTC6804_ADCOPT_1	0x1			//ADC Mode Low

#define LTC6804_MD_FAST		0x1			//ADC MODE 27Khx or 14Khz
#define LTC6804_MD_NORMAL	0x2			//ADC MODE 7Khz or 3Khz
#define LTC6804_MD_SLOW		0x3			//ADC MODE 26Hz og 2Khz

#define LTC6804_DCP_DISCHARGE_NOTPERMITTED 0x0	//Discharge on slave not permitted
#define LTC6804_DCP_DISCHARGE_PERMITTED 0x1			//Discharge on slave permitted


/*LTC6804 ICOM initial communication, defined by table 46 from datasheet */
#define LTC6804_ICOM_I2C_WRITE_START						0x6
#define LTC6804_ICOM_I2C_WRITE_STOP							0x1
#define LTC6804_ICOM_I2C_WRITE_BLANK						0x0
#define LTC6804_ICOM_I2C_WRITE_NOTRANSMIT				0x7
#define LTC6804_ICOM_SPI_WRITE_CSBLOW						0x8
#define LTC6804_ICOM_SPI_WRITE_CSBHIGH					0x9
#define LTC6804_ICOM_SPI_WRITE_NOTRANSMIT				0xF

#define LTC6804_ICOM_I2C_READ_START							0x6
#define LTC6804_ICOM_I2C_READ_STOP							0x1
#define LTC6804_ICOM_I2C_READ_SDALOW						0x0
#define LTC6804_ICOM_I2C_READ_SDAHIGH						0x7
#define LTC6804_ICOM_SPI_READ										0x7

/*LTC6804 FCOM final communication, defined by table 46 from datasheet */
#define LTC6804_FCOM_I2C_WRITE_MASTER_ACK				0x0
#define LTC6804_FCOM_I2C_WRITE_MASTER_NACK			0x8
#define LTC6804_FCOM_I2C_WRITE_MASTER_NACKSTOPP	0x9

#define LTC6804_FCOM_SPI_WRITE_CSBLOW						0x0
#define LTC6804_FCOM_SPI_WRITE_CSBHIGH					0x9

#define LTC6804_FCOM_I2C_READ_MASTER_ACK										0x0
#define LTC6804_FCOM_I2C_READ_SLAVE ACK											0x7
#define LTC6804_FCOM_I2C_READ_SLAVE_NACK										0xF
#define LTC6804_FCOM_I2C_READ_SLAVE_ACK_MASTER_STOPP				0x1
#define LTC6804_FCOM_I2C_READ_SLAVE_NACK_MASTER_STOPP				0x9

#define LTC6804_FCOM_SPI_READ										0xF

/*LTC1380 register*/
#define LTC1380_ADR_MASK 0x90
#define LTC1380_CMD_ENABLE 0x08

#define LTC1380_AD_0		0
#define LTC1380_AD_1		1
#define LTC1380_AD_2		2
#define LTC1380_AD_3		3

#define LTC1380_MUXAD_S0		0
#define LTC1380_MUXAD_S1		1
#define LTC1380_MUXAD_S2		2
#define LTC1380_MUXAD_S3		3
#define LTC1380_MUXAD_S4		4
#define LTC1380_MUXAD_S5		5
#define LTC1380_MUXAD_S6		6
#define LTC1380_MUXAD_S7		7

#define LTC1380_MUXAD(x) (\
	x == 0 ? LTC1380_MUXAD_S0 : /*LTC6804_AD_0*/ 	\
	x == 1 ? LTC1380_MUXAD_S1 : /*LTC6804_AD_1*/	\
	x == 2 ? LTC1380_MUXAD_S2 :	/*LTC6804_AD_2*/	\
	x == 3 ? LTC1380_MUXAD_S3 :	/*LTC6804_AD_3*/	\
	x == 4 ? LTC1380_MUXAD_S4 :	/*LTC6804_AD_4*/	\
	x == 5 ? LTC1380_MUXAD_S5 :	/*LTC6804_AD_5*/	\
	x == 6 ? LTC1380_MUXAD_S6 :	/*LTC6804_AD_6*/	\
	x == 7 ? LTC1380_MUXAD_S7 :	/*LTC6804_AD_7*/	\
	x == 8 ? LTC1380_MUXAD_S0 :	/*LTC6804_AD_8*/	\
	x == 9 ? LTC1380_MUXAD_S1 :	/*LTC6804_AD_9*/	\
	x == 10 ? LTC1380_MUXAD_S2:	/*LTC6804_AD_10*/	\
	x == 11 ? LTC1380_MUXAD_S3:	/*LTC6804_AD_11*/	\
	0x0007						/*LTC6804_ALL*/ 	\
)

#define LTC1380_COMMANDCODE_SETAD(sensor, write) \
				(LTC1380_ADR_MASK | ((sensor < 8) ? (LTC1380_AD_0 << 1) : (LTC1380_AD_1 << 1)) | \
				((write == TRUE) ? 0x01 : 0x00))
#define LTC1380_COMMANDCODE_SETMUXAD(sensor, enable) \
				LTC1380_MUXAD(sensor) | (sensor < 8) ? (sensor << 2) : ((sensor-7) << 2) | \
				((enable == TRUE) ? LTC1380_CMD_ENABLE : 0x00)

/*---Public Function prototypes----------------------------------------------------------------*/
/*No compiler code refered to this*/

#endif
