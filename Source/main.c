/*Dependencies-------------------------------------------------------------------------------*/

#define DEBUG_MODE

/*---------------------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
/*---------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "croutine.h"
/*---------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------*/
#include "Defines.h"
#include "Batterymanager.h"
#include "BMS_CAN.h"
#include "Buffer.h"
#include "System.h"
#include "ioMapping.h"
#include "watchdog.h"

/*---------------------------------------------------------------------------------*/

//#include "System.c"


 /* Circular buffer information struct */ 
 
#define NUMBER_OF_THREADS   3 

FLAG FLAGS[NUMBER_OF_THREADS] = {ASLEEP}; 

uint8_t delay = 0;

CanbusWriteBuffer_t bufferdata[CAN_REG_SIZE]; 
 

BMS_CAN_frame_t CanMessage[CAN_BUFFER_SIZE];


/*Function Prototypes-----------------------------------------------------------------------*/
void vWatchdog(void *pvParameters);
void vErrorInput(void *pvParameters);
void vBatteryManagement(void *pvParameters);
void vCanBus(void *pvParameters);
void Clock_config(void);
//uint8_t data[2] = {0x00,0x04};


#define BMS_TEST_FACILITY
#define STACK_SIZE_MIN	1000	/* usStackDepth	- the stack size DEFINED IN WORDS.*/

/*Program-----------------------------------------------------------------------------------*/
int main(void)
{
	SystemCoreClockUpdate();
	Batterymanager_init();
	BSP_CAN_init();

	CANBuffer_Init(CanMessage);
	
	for(uint8_t i=0;i<8;i++)
		{
			CanMessage[0].CanStructtekst.Name[i] = SYSTEM_NAME[i];
		}
	for(uint8_t i=0;i<8;i++)
		{
			CanMessage[1].CanStructtekst.Name[i] = SYSTEM_REV[i];
		}
	
	//bufferData_Init(bufferdata, CAN_REG_SIZE);
	/* Init the buffers */
	//buffer_init(&bufferCan1_s, bufferCan1, CAN_BUFFER_SIZE);
	
	/*!< At this stage the microcontroller clock setting is already configured,
	   this is done through SystemInit() function which is called from startup
	   file (startup_stm32f4xx.s) before to branch to application main.
	   To reconfigure the default setting of SystemInit() function, refer to
	   system_stm32f4xx.c file
	 */
	
	/*!< Most systems default to the wanted configuration, with the noticeable 
		exception of the STM32 driver library. If you are using an STM32 with 
		the STM32 driver library then ensure all the priority bits are assigned 
		to be preempt priority bits by calling 
		NVIC_PriorityGroupConfig( NVIC_PriorityGroup_4 ); before the RTOS is started.
	*/
	NVIC_PriorityGroupConfig( NVIC_PriorityGroup_4 );
	
  ioMapping_Init();
	
	xTaskCreate( vWatchdog, (const char*)"Watchdog", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL );
	xTaskCreate( vErrorInput, (const char*)"Error on input", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL );
	xTaskCreate( vBatteryManagement, (const char*)"BatteryManagement", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL );
	
	xTaskCreate( vCanBus, (const char*)"CANBus", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL );
	
	
	
	vTaskStartScheduler();
}
//******************************************************************************


//******************************************************************************
void vWatchdog(void *pvParameters)
{
    uint8_t xFlag;
    for(;;)
    {
        xFlag = 0;
        for(int i = 0;i < NUMBER_OF_THREADS;i++)
        {
            switch(FLAGS[i])
            {
                case UNKNOWN:
                    xFlag++;
                    break;
                case ALIVE:
                    FLAGS[i] = UNKNOWN;
                    break;
                case ASLEEP:
                    break;
            }
        }
        
        if(xFlag == 0)
            GPIO_ToggleBits(GPIOA, GPIO_Pin_8);
        taskYIELD();	
    }
}

void vErrorInput(void *pvParameters)
{
	FLAG* flg = &FLAGS[0];
	for(;;)
	{
		if(delay < 10)
		{
			delay++;
		}else
		{
		*flg = ALIVE;

		Error_DigitalInput();
		
		*flg = ASLEEP;
		}
		vTaskDelay( 300 / portTICK_RATE_MS );
		
	}
}

void vBatteryManagement(void *pvParameters)
{
	FLAG* flg = &FLAGS[1];
	for(;;)
	{
		*flg = ALIVE;
		//CanMessage[3].FullFrame.Write = 1;
		//Voltage_Monitor_Task();
		Battery_Monitor_Task();
		//GPIO_ToggleBits(GPIOD, GPIO_Pin_13);
		*flg = ASLEEP;
		vTaskDelay( 250 / portTICK_RATE_MS );
	}
}

void vCanBus(void *pvParameters)
{
	FLAG* flg = &FLAGS[2];
	for(;;)
	{
		*flg = ALIVE;
		GPIO_ToggleBits(GPIO_LED4, PIN_LED4);
		if(CanMessage[3].FullFrame.Write == 0)
		{
			CanTxMsg Message;
			Message.StdId = CAN_BMS_NAME;
			Message.ExtId = 0;
			Message.RTR = 0;
			Message.DLC = 8;
			Message.IDE = 0;
			for(uint8_t j=0;j<CAN_BUFFER_SIZE;j++)
			{

				Message.StdId = CanMessage[j].FullFrame.ID;
				for(uint8_t i = 0;i<Message.DLC;i++)
				{
					Message.Data[i] = CanMessage[j].FullFrame.Data[i];
				}
			
			while(CAN_Transmit(CAN1, &Message) == CAN_TxStatus_NoMailBox);
			}
			CanMessage[3].FullFrame.Write = 1;
		}
		*flg = ASLEEP;
		vTaskDelay( 1000 / portTICK_RATE_MS );
	}
}

/**
* @brief : IRQ-handler for message recieved on CAN1
* @note : CAN1 is configured to recieve messages on FIFO0
*/
void CAN1_RX0_IRQHandler(void)
{
	CanRxMsg rxmsg;
  CAN_Receive(CAN1, CAN_FIFO0, &rxmsg);
  //buffer_write(&bufferCan1_s, rxmsg);
}
//******************************************************************************
