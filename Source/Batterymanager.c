
/*Dependencies------------------------------------------------------------------------------*/
#include "Batterymanager.h"


void Temperature_Task_Init(void);
void Voltage_Task_Init(void);
void Balancer_Task_Init(void);
void Current_Task_Init(void);
void Fan_Task_Init(void);
void Fan_control(BOOLEAN Active, uint16_t temperature);

void Batterymanager_init(void)
{
	LTC6804_Init();
	Temperature_Task_Init();
	Fan_Task_Init();
	Voltage_Task_Init();
	Balancer_Task_Init();
	Current_Task_Init();

}

void Temperature_Task_Init(void)
{
	Temperature_Init();
	Temperature_Wakeup_Init();
}

void Fan_Task_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
		/* Enable the GPIO_LED Clock */
	RCC_AHB1PeriphClockCmd(RCC_PWM_ENABLE, ENABLE);

	/* Configure the GPIO_LED pin */
	GPIO_InitStructure.GPIO_Pin = PIN_PWM_ENABLE ;//GPIO_Pin_2 | GPIO_Pin_4 | GPIO_Pin_5 ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIO_PWM_ENABLE, &GPIO_InitStructure);
	
	
	PWM_Init(GPIO_FAN, PIN_FAN1, PIN_FAN2, PIN_FAN3, PIN_FAN4);
	
}

void Voltage_Task_Init(void)
{
		Voltage_Init();
}

void Balancer_Task_Init(void)
{
		
}

void Current_Task_Init(void)
{
		current_Init();
}

void Battery_Monitor_Task(void)
{
	/*Readings*/
	GPIO_ToggleBits(GPIO_LED2, PIN_LED2);
	//Voltage_Monitor_Task();
	delay_us(10);
	Temperature_Monitor_Task();
	
	//Current_Monitor_Task();
	
	/*Calculations*/
	
	
	/*Evaluation*/
	
	/*Output*/
	//Fan_control(TRUE, 1000);
}


void Voltage_Monitor_Task(void)
{
	Voltage_Monitor();
	GPIO_SetBits(GPIO_PWM_ENABLE, PIN_PWM_ENABLE);
	PWM_Speed(100);
}

void Temperature_Monitor_Task(void)
{
		Temperature_Monitor();
		GPIO_SetBits(GPIO_PWM_ENABLE, PIN_PWM_ENABLE);
		PWM_Speed(100);
}

void Current_Monitor_Task(void)
{
		
}

void Balancer_Control_Task(void)
{
		
}

void Fan_control(BOOLEAN Active, uint16_t temperature)	
{
	if(temperature < LOWTEMP)
	{
		Active = FALSE;
	}	
	
	if(Active == TRUE)
	{
		if(temperature > HIGHTEMP)
		{
			PWM_Speed(PWM_PERIOD);
			
		} 
		else
		{
			PWM_Speed(temperature/HIGHTEMP * PWM_PERIOD);		
		}
		
	}
	else
	{
		PWM_Speed(0);
	}
	
}


