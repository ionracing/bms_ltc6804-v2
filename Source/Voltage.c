

/*---Dependencies----------------------------------------------------------------*/
#include "Voltage.h"
#include "Extern_Global_Values.h"


/*---Local Driver properties----------------------------------------------------------------*/
#define VOLTAGE_VUV_DEFAULT         CELL_MINVOLTAGE		
#define VOLTAGE_VOV_DEFAULT         CELL_MAXVOLTAGE		


uint16_t voltage_cell[SYSTEM_MAX_BOARDS][SYSTEM_NUM_CELLS];
uint16_t voltage_sum[SYSTEM_MAX_BOARDS];

uint16_t voltage_vov_threshold;                                    // over-voltage threshold in LTC6804_VOLTAGE_RESOLUTION.
uint16_t voltage_vuv_threshold;
uint16_t voltage_ov_flags[SYSTEM_MAX_BOARDS];
uint16_t voltage_uv_flags[SYSTEM_MAX_BOARDS];

/*---Public Function prototypes----------------------------------------------------------------*/
void Voltage_Init(void)
{
	
	// Init thresholds to defaults.
	voltage_vov_threshold = VOLTAGE_VOV_DEFAULT;
	voltage_vuv_threshold = VOLTAGE_VUV_DEFAULT;
	
	
}

void Voltage_Monitor(void)
{
//	uint8_t board_num;
	uint8_t cell_num;
	uint16_t voltage_temp[SYSTEM_NUM_CELLS];
//	uint16_t vov_flags, vuv_flags;
//	BOOLEAN suspend_sent;
	BOOLEAN results_clear;
	uint32_t cell_sum;
	uint32_t Pack_sum;
	
	uint16_t MinV	=	0xFFFF;
	uint16_t MinV_ID;
	uint16_t MaxV	=	0x0000;
	uint16_t MaxV_ID;
//	static uint8_t interate = 0;
	
	for(uint8_t pack_num = 0; pack_num < SYSTEM_MAX_BOARDS;pack_num++)
	{
//		if(interate % 2 == 0)
//		{
//			LTC6804_Voltage_Clear(pack_num);
//		}
	}
//	interate++;
	delay_us(20);
	for(uint8_t pack_num = 0; pack_num < SYSTEM_MAX_BOARDS;pack_num++)
	{
		LTC6804_Voltage_Start(pack_num, TRUE);
	}	
	//wakeup_timestampNew = xTaskGetTickCount();
	
	delay_us(3200);
	
	for(uint8_t pack_num = 0; pack_num < SYSTEM_MAX_BOARDS; pack_num++)
	{
		results_clear = FALSE;
		cell_sum = 0;
		
		if(LTC6804_Voltage_Read(pack_num,voltage_temp) == TRUE)
		{
			for(cell_num = 0; cell_num < SYSTEM_NUM_CELLS; cell_num++)
			{
				if(voltage_temp[cell_num] != LTC6804_ADC_CLEAR)
				{
					voltage_cell[pack_num][cell_num] = voltage_temp[cell_num];
					cell_sum += voltage_cell[pack_num][cell_num];
					/*Find the lowest and highest voltage value.*/
					if(voltage_temp[cell_num] < MinV)
					{
						MinV = voltage_temp[cell_num];
						MinV_ID = cell_num + 1;
					}
					if(voltage_temp[cell_num] > MaxV)
					{
						MaxV = voltage_temp[cell_num];
						MaxV_ID = cell_num + 1;
					}
				}else
				{
					results_clear = TRUE;
				}
			}
			GPIO_ToggleBits(GPIO_LED2, PIN_LED2);
			
			if(results_clear == FALSE)
			{
				voltage_sum[pack_num] = cell_sum;
				Pack_sum += cell_sum;
				
			}else
			{
				GPIO_ResetBits(GPIO_BMS_ERR, PIN_BMS_ERR);
				GPIO_ToggleBits(GPIO_LED3, PIN_LED3);
				/*Error*/
				
			}
			
//			if((System_Powered_Up() == TRUE) && (results_clear == FALSE))
//			{
//				vov_flags = voltage_uv_flags[pack_num];
//				vuv_flags = voltage_ov_flags[pack_num];
//			}	
		}else
			{
				GPIO_ResetBits(GPIO_BMS_ERR, PIN_BMS_ERR);
				GPIO_ToggleBits(GPIO_LED3, PIN_LED3);
			}	
	}
	
	
	//voltage_cell[SYSTEM_MAX_BOARDS][SYSTEM_NUM_CELLS];
	if(CanMessage[3].FullFrame.Write == 1 )
	{
		//#if defined BMS_TEST_FACILITY
		for(uint8_t k = 0;k<SYSTEM_MAX_BOARDS;k++)
		{
			
			CANBuffer_copy(CanMessage,voltage_cell[k],k,9,1000);

		}
		//#endif
		
		/*Set data for Voltage updates*/
		CanMessage[3].CanStructCustomFrame.d0 = (uint16_t)Pack_sum/1000;
		CanMessage[3].CanStructCustomFrame.d2 = (uint8_t)(MinV/1000);
		CanMessage[3].CanStructCustomFrame.d3 = (uint8_t)MinV_ID;
		CanMessage[3].CanStructCustomFrame.d4 = (uint8_t)(MaxV/1000);
		CanMessage[3].CanStructCustomFrame.d5 = (uint8_t)MaxV_ID;
		CanMessage[3].CanStructCustomFrame.Write = 0;
		CanMessage[3].FullFrame.Write = 0;
	}
	/*
	
	if(bufferdata[3].write == 1)
	{
		bufferdata[3].Data[0] = (uint8_t)(Pack_sum >> 8 & 0x00FF);
		bufferdata[3].Data[1] = (uint8_t)(Pack_sum & 0x00FF);
		bufferdata[3].Data[2] = (uint8_t)MinV;
		bufferdata[3].Data[3] = (uint8_t)MinV_ID;
		bufferdata[3].Data[4] = (uint8_t)MaxV;
		bufferdata[3].Data[5] = (uint8_t)MaxV_ID;
		bufferdata[3].read = 1;
		bufferdata[3].write = 0;
	}

	uint8_t cnt = 0;
	uint8_t cnt2 = 0;
	if(	bufferdata[8 + cnt2 + CAN_BASE_ID].write == 1)
	{
		for(uint8_t cell_num = 0; cell_num < 12;cell_num++)
		{
			bufferdata[8 + cnt2 + CAN_BASE_ID].Data[2 * cnt] = (uint8_t)(voltage_cell[0][cell_num] >> 8 & 0x00FF);
			bufferdata[8 + cnt2 + CAN_BASE_ID].Data[2*cnt + 1] = (uint8_t)(voltage_cell[0][cell_num] & 0x00FF);
			cnt++;
			if(!(cnt<4))
			{
				bufferdata[8 + cnt2 + CAN_BASE_ID].read = 1;
				bufferdata[8 + cnt2 + CAN_BASE_ID].write = 0;
				cnt = 0;
				cnt2++;
			}
		}
	}
	*/
	delay_us(2400);
	
}

void Voltage_Manager(void)
{

	
	
	
	
}

/*---Local Function prototypes----------------------------------------------------------------*/
