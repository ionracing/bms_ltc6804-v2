
#include "SPI.h"

void SPI_initialise(SPI_TypeDef* SPIx, SPI_SPEED speed)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    SPI_InitTypeDef SPI_InitStruct;
    
    static uint8_t SPI1_ENABLED = 0;
    static uint8_t SPI2_ENABLED = 0;
    static uint8_t SPI3_ENABLED = 0;
    
    if(SPIx == SPI1)
    {
        if(SPI1_ENABLED)
            return;
        SPI1_ENABLED = 1;
    }
    else if(SPIx == SPI2)
    {
        if(SPI2_ENABLED)
            return;
        SPI2_ENABLED = 1;
    }
    else if(SPIx == SPI3)
    {
        if(SPI3_ENABLED)
            return;
        SPI3_ENABLED = 1;
    }
    RCC_AHB1PeriphClockCmd(SPI_RCC_GPIO(SPIx), ENABLE);
    if(SPI_GPIOx(SPIx) != SPI_GPIOx_CS(SPIx))
        RCC_AHB1PeriphClockCmd(SPI_RCC_GPIO_CS(SPIx), ENABLE);
    SPI_APB_SPI(SPIx);
    
	GPIO_PinAFConfig(SPI_GPIOx(SPIx), SPI_GPIO_PinSource_SCLK(SPIx), SPI_GPIO_AF_SPI(SPIx));
	GPIO_PinAFConfig(SPI_GPIOx(SPIx), SPI_GPIO_PinSource_MISO(SPIx), SPI_GPIO_AF_SPI(SPIx));
	GPIO_PinAFConfig(SPI_GPIOx(SPIx), SPI_GPIO_PinSource_MOSI(SPIx), SPI_GPIO_AF_SPI(SPIx));
   

		
	GPIO_InitStruct.GPIO_Pin =  SPI_GPIO_Pin_SCLK(SPIx) | SPI_GPIO_Pin_MOSI(SPIx);
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(SPI_GPIOx(SPIx), &GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin =  SPI_GPIO_Pin_MISO(SPIx);
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(SPI_GPIOx(SPIx), &GPIO_InitStruct);
    
    GPIO_InitStruct.GPIO_Pin =  SPI_GPIO_Pin_CS(SPIx);
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(SPI_GPIOx_CS(SPIx), &GPIO_InitStruct);
    
    SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master;
	SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStruct.SPI_CPOL = SPI_CPOL_High;
	SPI_InitStruct.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;
	SPI_SetSpeed(SPIx, &SPI_InitStruct, speed);	
	SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_Init(SPIx, &SPI_InitStruct);
    
    SPI_Cmd(SPIx, ENABLE);
}

void SPI_SetSpeed(SPI_TypeDef* SPIx, SPI_InitTypeDef* SPI_InitStruct, SPI_SPEED speed)
{
    
    
    switch(speed)
    {
        case 0: //42MHz
            if(SPIx == SPI1)
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
            else
                while(1); // SPI2 and SPI3, cant work on this speed.
						break;
        case 1: //21MHz
            if(SPIx == SPI1)
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
            else
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
						break;
        case 2: //10MHz
            if(SPIx == SPI1)
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
            else
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
						break;
        case 3: //5MHz
            if(SPIx == SPI1)
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;
            else
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
						break;
        case 4: //2600KHz
            if(SPIx == SPI1)
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;
            else
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;
						break;
        case 5: //1300KHz
            if(SPIx == SPI1)
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64;
            else
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32;
						break;
        case 6: //650KHz
            if(SPIx == SPI1)
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128;
            else
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64;
						break;
        case 7: //320KHz
            if(SPIx == SPI1)
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
            else
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128;
						break;
        case 8: //160KHz
            if(SPIx == SPI1)
                while(1); // SPI1, cant work on this speed.
            else
                SPI_InitStruct->SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
						break;
    }

}

uint8_t SPI_Read(SPI_TypeDef* SPIx)
{
    while(SPIx->SR & SPI_I2S_FLAG_BSY);         // wait until SPI is not busy anymore
    while(!(SPIx->SR & SPI_I2S_FLAG_RXNE));     // wait until receive complete
	return SPI_ReceiveData(SPIx);
}

void SPI_Write_Array(SPI_TypeDef* SPIx, uint8_t *Data, uint8_t num_element)
{
	for(uint8_t number = 0; number < num_element;number++)
	{
		SPI_Write(SPIx, Data[number]);
	}
}

void SPI_Write(SPI_TypeDef* SPIx, uint8_t Data)
{
    while(SPIx->SR & SPI_I2S_FLAG_BSY);         // wait until SPI is not busy anymore
    while(!(SPIx->SR & SPI_I2S_FLAG_TXE));      // wait until transmit complete
    SPI_SendData(SPIx, Data);
    while(!(SPIx->SR & SPI_I2S_FLAG_RXNE));     // wait until receive complete
}

uint8_t SPI_WriteRead(SPI_TypeDef* SPIx, uint8_t msg)
{
    SPI_Write(SPIx, msg);
    return SPI_Read(SPIx);
}
