
/*---Dependencies----------------------------------------------------------------*/
#include "Temperature.h"
#include "Extern_Global_Values.h"

/*---Local Driver properties----------------------------------------------------------------*/
#define TEMPERATURE_TABLE_IDX_MAX 			42
#define TEMPERATURE_TABLE_RESOLUTION    5 

uint8_t temperature_in_process; 
uint8_t temperature_board_for_adc_values;
uint16_t temperature_adc_values[SYSTEM_NUM_TEMP];
BOOLEAN temperature_skip;
uint16_t temperatures[SYSTEM_MAX_BOARDS][SYSTEM_NUM_TEMP];

uint8_t temperature_lookup(uint16_t adc_value);
uint8_t cnt = 0;
uint8_t cnt2 = 0;


const uint16_t Temperature_Table[] = { 49486,  // -55?C
																			 49264,  // -50?C
																			 48962,  // -45?C
																			 48556,  // -40?C
																			 48020,  // -35?C
																			 47326,  // -30?C
																			 46438,  // -25?C
																			 45330,  // -20?C
																			 43970,  // -15?C
																			 42346,  // -10?C
																			 40443,  // -5?C
																			 38276,  // 0?C
																			 35871,  // 5?C
																			 33277,  // 10?C
																			 30552,  // 15?C
																			 27767,  // 20?C
																			 25000,  // 25?C
																			 22309,   // 30?C
																			 19753,   // 35?C
																			 17377,   // 40?C
																			 15202,   // 45?C
																			 13243,   // 50?C
																			 11496,   // 55?C
																			 9961,   // 60?C
																			 8619,    // 65?C
																			 7454,    // 70?C
																			 6449,    // 75?C
																			 5587,    // 80?C
																			 4841,    // 85?C
																			 4202,    // 90?C
																			 3654,    // 95?C
																			 3183,    // 100?C
																			 2779,    // 105?C
																			 2431,    // 110?C
																			 2132,    // 115?C
																			 1873,    // 120?C
																			 1652,    // 125?C
																			 1460,    // 130?C
																			 1292,    // 135?C
																			 1147,    // 140?C
																			 1020,    // 145?C
																			 909,    // 150?C
																			 813 } ; // 155?C

/*---Public Function prototypes----------------------------------------------------------------*/
void Temperature_Init(void)
{
	temperature_in_process = 0;
	temperature_board_for_adc_values = 0;

}

BOOLEAN Temperature_Wakeup_Init(void)
{
	BOOLEAN success = TRUE;
//	uint8_t refon_temp;
	uint8_t pack_num;
	
	
	for(pack_num = 0; pack_num < SYSTEM_MAX_BOARDS; pack_num++)
	{
		LTC6804_Refon_Set(pack_num, TRUE);
	}
	
	temperature_skip = TRUE;
	return success;
}

void Temperature_Monitor(void)
{
	uint8_t pack_num;
	uint16_t adc_value;
	uint16_t MinT;
	uint16_t MinT_ID;
	uint16_t MaxT;
	uint16_t MaxT_ID;
	
	if(temperature_in_process == 0)
	{
		uint16_t MinT	=	0xFFFF;
		uint16_t MaxT	=	0x0000;
	}	else
	{
		uint16_t MinT	= CanMessage[8].CanStructCustomFrame.d2;
		uint16_t MinT_ID = CanMessage[8].CanStructCustomFrame.d3;
		uint16_t MaxT	= CanMessage[8].CanStructCustomFrame.d4;
		uint16_t MaxT_ID = CanMessage[8].CanStructCustomFrame.d5;
	}
		
	/*Delete adc register for the temperature*/
	for(pack_num = 0; pack_num < SYSTEM_MAX_BOARDS; pack_num++)
	{
		//LTC6804_GPIO_Clear(pack_num);
	}
	
	
	for(pack_num = 0; pack_num < SYSTEM_MAX_BOARDS; pack_num++)
	{
		LTC6804_GPIO_Start(pack_num);
	}
	
	if(temperature_skip == TRUE)
	{
		temperature_skip = FALSE;
	}else
	{
		delay_us(405);	//7KHz ADC conversion


		
		for(pack_num = 0; pack_num < SYSTEM_MAX_BOARDS; pack_num++)
		{
			if(LTC6804_GPIO_Read(pack_num, &adc_value) == TRUE)
			{
				if(adc_value != LTC6804_ADC_CLEAR)
				{
					/*
					if(pack_num == temperature_board_for_adc_values)
					{
							temperature_adc_values[temperature_in_process] = adc_value;						
					}
					*/
					temperatures[pack_num][temperature_in_process] =  temperature_lookup(adc_value);					
					/*Find the lowest and highest temperature value.*/
					if(temperatures[pack_num][temperature_in_process] < MinT)
					{
						MinT = temperatures[pack_num][temperature_in_process];
						MinT_ID = pack_num + 1;
					}
					if(temperatures[pack_num][temperature_in_process] > MaxT)
					{
						MaxT = temperatures[pack_num][temperature_in_process];
						MaxT_ID = pack_num + 1;
					}
					/*
					if(pack_num == 0)
					{
						if(!(cnt<4))
						{
							bufferdata[11 + cnt2 + CAN_BASE_ID].write = 0;
							bufferdata[11 + cnt2 + CAN_BASE_ID].read = 1;
							cnt = 0;
							cnt2++;
							if(!(cnt2 <3))
							{
								cnt2 = 0;
							}
						}
						bufferdata[11 + cnt2 + CAN_BASE_ID].Data[2 * cnt] = (uint8_t)adc_value >> 8;
						bufferdata[8 + cnt2 + CAN_BASE_ID].Data[2*cnt + 1] = (uint8_t)adc_value;
					
					}*/
				}else
					{// Error on reading
						GPIO_ResetBits(GPIO_BMS_ERR, PIN_BMS_ERR);
						GPIO_ToggleBits(GPIO_LED3, PIN_LED3);
					}	
				
			
			}else
			{// Error on reading
				GPIO_ResetBits(GPIO_BMS_ERR, PIN_BMS_ERR);
				GPIO_ToggleBits(GPIO_LED3, PIN_LED3);
			}			
		}
		
		for(pack_num = 0; pack_num < SYSTEM_MAX_BOARDS; pack_num++)
			{
				LTC6804_I2C_Write(pack_num, temperature_in_process, FALSE);
			}	
		
		
		if(temperature_in_process < (SYSTEM_NUM_TEMP -1))
		{
			temperature_in_process++;
		}else
		{
			temperature_in_process = 0;
		
		if(CanMessage[3].FullFrame.Write == 1 )
		{	
			//#if defined BMS_TEST_FACILITY
		for(uint8_t k = 0;k<SYSTEM_MAX_BOARDS;k++)
		{
			
			CANBuffer_copy(CanMessage,temperatures[k],k,9,1);

		}
		CanMessage[3].CanStructCustomFrame.Write = 0;
		CanMessage[3].FullFrame.Write = 0;
		
		}
		//#endif
		

		GPIO_ToggleBits(GPIO_LED1, PIN_LED1);	
		
		}

				/*Set data for Voltage updates*/
		CanMessage[8].CanStructCustomFrame.d2 = (uint8_t)((MinT));
		CanMessage[8].CanStructCustomFrame.d3 = (uint8_t)MinT_ID;
		CanMessage[8].CanStructCustomFrame.d4 = (uint8_t)((MaxT));
		CanMessage[8].CanStructCustomFrame.d5 = (uint8_t)MaxT_ID;
	}
	for(pack_num = 0; pack_num < SYSTEM_MAX_BOARDS; pack_num++)
	{
		LTC6804_I2C_Write(pack_num, temperature_in_process, TRUE);
	}
	
}

/*---Local Function prototypes----------------------------------------------------------------*/
uint8_t temperature_lookup(uint16_t adc_value)
{
    uint8_t idx, idx_min, idx_max;
    uint16_t temp_uint16, temp_uint16b;

    if(Temperature_Table[0] <= adc_value)
    {
        // Temperature is too low for the table, return min value
        idx =  0;
    }
    else if(Temperature_Table[TEMPERATURE_TABLE_IDX_MAX] >= adc_value)
    {
        // Temperature is too high for the table, return max value
        idx = TEMPERATURE_TABLE_IDX_MAX*TEMPERATURE_TABLE_RESOLUTION;
    }
    else
    {
        idx_min = 0;
        idx_max = TEMPERATURE_TABLE_IDX_MAX;
        idx = TEMPERATURE_TABLE_IDX_MAX/2;

        // Temperature is in the table.  Find the closest values and interpolate
        while (idx_min < idx_max)
        {
            temp_uint16 = Temperature_Table[idx];

            if(temp_uint16 == adc_value)
            {
                // If temperature is exactly value in the table, stop searching
                break;
            }
            else if(temp_uint16 < adc_value)
            {
                // If temperature is lower than tested table value, adjust the max
                idx_max = idx;
            }
            else
            {
                // If temperature is higher than tested table value, adjust the min
                idx_min = idx + 1;
            }

            idx = (idx_max + idx_min) >> 1;
        }

        // idx is the value in the table that's higher than the measured temperature.  Interpolate to nearest degree.
        temp_uint16 = (Temperature_Table[idx - 1] - Temperature_Table[idx]);
        temp_uint16b = (Temperature_Table[idx - 1] - adc_value);
        idx = ((idx - 1) * TEMPERATURE_TABLE_RESOLUTION) + (temp_uint16b * TEMPERATURE_TABLE_RESOLUTION + temp_uint16/2) / temp_uint16;
    }

    return idx;
}
