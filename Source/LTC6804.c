

/*---Dependencies----------------------------------------------------------------*/
#include "LTC6804.h"



/*---Local Driver properties----------------------------------------------------------------*/
/*LTC6804 timing table*/
#define LTC6804_TWAKE                                           300     // us, max value
#define LTC6804_TSLEEP                                          1800    // ms, min value
#define LTC6804_TREADY                                          10      // us, max value
#define LTC6804_TIDLE                                           4300    // us, min value
#define LTC6804_TDWELL                                          1    

/*---Local Function prototypes----------------------------------------------------------------*/
void wakeup (void);
void ltc6804_send_commandcode(uint16_t command_code, BOOLEAN reg_group_command);
void ltc6804_cfgr_modify(uint8_t Pack_num, uint8_t *register_mask_ptr, uint8_t *register_value_ptr);
BOOLEAN ltc6804_register_group_read(uint8_t *register_group);
void ltc6804_register_group_write(uint8_t *register_group);
uint16_t LTC6804_PEC_Calc(uint8_t *data, uint8_t length);
static uint16_t ltc6804_pec_lookup(uint8_t data, uint16_t remainder);

/*---Local Variables----------------------------------------------------------------*/
uint32_t ltc6804_wakeup_timestamp;
uint8_t ltc6804_gpio_pulldown; 

/*PEC calculation------------------------------*/
uint16_t pecSeedValue = 16;
uint16_t pecTable[256] = {  0x0000, 0xc599, 0xceab, 0x0b32, 0xd8cf, 0x1d56, 0x1664, 0xd3fd, 0xf407, 0x319e, 0x3aac,
    0xff35, 0x2cc8, 0xe951, 0xe263, 0x27fa, 0xad97, 0x680e, 0x633c, 0xa6a5, 0x7558, 0xb0c1,
    0xbbf3, 0x7e6a, 0x5990, 0x9c09, 0x973b, 0x52a2, 0x815f, 0x44c6, 0x4ff4, 0x8a6d, 0x5b2e,
    0x9eb7, 0x9585, 0x501c, 0x83e1, 0x4678, 0x4d4a, 0x88d3, 0xaf29, 0x6ab0, 0x6182, 0xa41b,
    0x77e6, 0xb27f, 0xb94d, 0x7cd4, 0xf6b9, 0x3320, 0x3812, 0xfd8b, 0x2e76, 0xebef, 0xe0dd,
    0x2544, 0x02be, 0xc727, 0xcc15, 0x098c, 0xda71, 0x1fe8, 0x14da, 0xd143, 0xf3c5, 0x365c,
    0x3d6e, 0xf8f7, 0x2b0a, 0xee93, 0xe5a1, 0x2038, 0x07c2, 0xc25b, 0xc969, 0x0cf0, 0xdf0d,
    0x1a94, 0x11a6, 0xd43f, 0x5e52, 0x9bcb, 0x90f9, 0x5560, 0x869d, 0x4304, 0x4836, 0x8daf,
    0xaa55, 0x6fcc, 0x64fe, 0xa167, 0x729a, 0xb703, 0xbc31, 0x79a8, 0xa8eb, 0x6d72, 0x6640,
    0xa3d9, 0x7024, 0xb5bd, 0xbe8f, 0x7b16, 0x5cec, 0x9975, 0x9247, 0x57de, 0x8423, 0x41ba,
    0x4a88, 0x8f11, 0x057c, 0xc0e5, 0xcbd7, 0x0e4e,  0xddb3, 0x182a, 0x1318, 0xd681, 0xf17b,
    0x34e2, 0x3fd0, 0xfa49, 0x29b4, 0xec2d, 0xe71f, 0x2286, 0xa213, 0x678a, 0x6cb8, 0xa921,
    0x7adc, 0xbf45, 0xb477, 0x71ee, 0x5614, 0x938d, 0x98bf, 0x5d26, 0x8edb, 0x4b42, 0x4070,
    0x85e9, 0x0f84, 0xca1d, 0xc12f, 0x04b6, 0xd74b, 0x12d2, 0x19e0, 0xdc79, 0xfb83, 0x3e1a, 0x3528,
    0xf0b1, 0x234c, 0xe6d5, 0xede7, 0x287e, 0xf93d, 0x3ca4, 0x3796, 0xf20f, 0x21f2, 0xe46b, 0xef59,
    0x2ac0, 0x0d3a, 0xc8a3, 0xc391, 0x0608, 0xd5f5, 0x106c, 0x1b5e, 0xdec7, 0x54aa, 0x9133, 0x9a01,
    0x5f98, 0x8c65, 0x49fc, 0x42ce, 0x8757, 0xa0ad, 0x6534, 0x6e06, 0xab9f, 0x7862, 0xbdfb, 0xb6c9,
    0x7350, 0x51d6, 0x944f, 0x9f7d, 0x5ae4, 0x8919, 0x4c80, 0x47b2, 0x822b, 0xa5d1, 0x6048, 0x6b7a,
    0xaee3, 0x7d1e, 0xb887, 0xb3b5, 0x762c, 0xfc41, 0x39d8, 0x32ea, 0xf773, 0x248e, 0xe117, 0xea25,
    0x2fbc, 0x0846, 0xcddf, 0xc6ed, 0x0374, 0xd089, 0x1510, 0x1e22, 0xdbbb, 0x0af8, 0xcf61, 0xc453,
    0x01ca, 0xd237, 0x17ae, 0x1c9c, 0xd905, 0xfeff, 0x3b66, 0x3054, 0xf5cd, 0x2630, 0xe3a9, 0xe89b,
    0x2d02, 0xa76f, 0x62f6, 0x69c4, 0xac5d, 0x7fa0, 0xba39, 0xb10b, 0x7492, 0x5368, 0x96f1, 0x9dc3,
    0x585a, 0x8ba7, 0x4e3e, 0x450c, 0x8095
};

/*---Public Function----------------------------------------------------------------*/

/*Initialize settings for LTC6804 driver*/
void LTC6804_Init(void) 
{
	/*Initialise the LTC driver*/
	ltc6804_gpio_pulldown = ((1 << LTC6804_NUM_GPIO) - 1);
	/*Initialise Serial interface for LTC chip*/
	
	SPI_initialise(SPI2,SPI_SPEED_320KHz);
	//SPI_SetSpeed(SPI2,SPI_SPEED_1300KHz);
}

/*Initialize GPIO settings*/
void LTC6804_GPIO_Init(uint8_t pack_num)
{
//	uint8_t adcopt;
	
}

void Global_Wakeup(void)
{
	wakeup();
}
// Read/Modify/Write the Config to turn the Reference on and off.
void LTC6804_Refon_Set(uint8_t pack_num, BOOLEAN refon)
{
    struct {
        uint8_t mask[LTC6804_REGISTER_GROUP_SIZE];
        uint8_t value[LTC6804_REGISTER_GROUP_SIZE];
    } register_modify;

    // Start with clear mask and value, and then set up gpio mask/value
		for (uint8_t i = 1; i < LTC6804_REGISTER_GROUP_SIZE;i++)
		{
			register_modify.mask[i] = 0;
		}

    register_modify.mask[0] = 0x1F << 3;
    register_modify.value[0] = (ltc6804_gpio_pulldown & 0x1F) << 3;

    // Create Mask to set Ref On
    register_modify.mask[0] |= 0x1 << 2;

    // Set Value for Ref
    register_modify.value[0] |= (refon ? 1 : 0) << 2;

    ltc6804_cfgr_modify(pack_num, register_modify.mask, register_modify.value);
}

void LTC6804_UVOV_Thresholds_Set(uint8_t pack_num, uint16_t vuv_value, uint16_t vov_value)
{
    struct {
        uint8_t mask[LTC6804_REGISTER_GROUP_SIZE];
        uint8_t value[LTC6804_REGISTER_GROUP_SIZE];
    } register_modify;

    //Start with clear mask and value, and then set up gpio mask/value
		for (uint8_t i = 1; i < LTC6804_REGISTER_GROUP_SIZE;i++)
		{
			register_modify.mask[i] = 0;
		}
    register_modify.mask[0] = 0x1F << 3;
    register_modify.value[0] = (ltc6804_gpio_pulldown & 0x1F) << 3;

    // Create Mask to set VUV and VOV
    *((uint16_t*)(register_modify.mask+1)) |= 0xFFF;
    *((uint16_t*)(register_modify.mask+2)) |= 0xFFF << 4;

    // Set Value for VUV and VOV
    *((uint16_t*)(register_modify.value+1)) |= (vuv_value & 0xFFF);
    *((uint16_t*)(register_modify.value+2)) |= (vov_value & 0xFFF) << 4;

    ltc6804_cfgr_modify(pack_num, register_modify.mask, register_modify.value);
}

void LTC6804_Discarge_Set(uint8_t pack_num, uint16_t discharge_bitmap, uint16_t timeout_value)
{
	
	struct 
	{
		uint8_t mask[LTC6804_REGISTER_GROUP_SIZE];
		uint8_t value[LTC6804_REGISTER_GROUP_SIZE];
	} register_modify;
	
	for(uint8_t i = 1; i < LTC6804_REGISTER_GROUP_SIZE;i++)
	{
		register_modify.mask[i] = 0;
	}
	
	register_modify.mask[0] = 0x1F << 3;
	register_modify.value[0] = (ltc6804_gpio_pulldown & 0x1F) << 3;

	*((uint16_t*)(register_modify.mask + 4)) |= 0xFFF;
	register_modify.mask[5] |= 0xF;
	
	// Set Value for discharger and discharge timeout values
	*((uint16_t*)(register_modify.value+4)) |= (discharge_bitmap & 0xFFF);
	register_modify.value[5] |= (timeout_value & 0xF) << 4;
	
	ltc6804_cfgr_modify(pack_num, register_modify.mask, register_modify.value);
	
	
}

/*Clear GPIO on LTC6804*/
void LTC6804_GPIO_Clear(uint8_t pack_num)
{
	uint8_t address;
	uint16_t command_code;
	
	address = LTC6804_GET_BOARD_ADDRESS(pack_num);
	/*Wakeup communication between master and slave*/
	wakeup();
	
	/*create the clear AUX command code*/
	command_code = LTC6804_COMMANDCODE_CLRAUX(address);

	
	ltc6804_send_commandcode(command_code, FALSE);
}

/*Start read of GPIO on LTC6804*/
void LTC6804_GPIO_Start(uint8_t pack_num)
{
	uint8_t address;
	uint16_t command_code;
	uint8_t md = 0x2;		//Normal Reading mode
	uint8_t gpio_select = 2;
	
	address = LTC6804_GET_BOARD_ADDRESS(pack_num);
	
	/*Wakeup communication between master and slave*/
	wakeup();	
	
	command_code = LTC6804_COMMANDCODE_ADAX(address, md, gpio_select);
	
	ltc6804_send_commandcode(command_code,FALSE);
	
}

/*Read GPIO port from LTC6804*/
BOOLEAN LTC6804_GPIO_Read(uint8_t pack_num, uint16_t *adc_value_ptr)
{
	BOOLEAN success = TRUE;
	uint8_t address;
	uint16_t command_code;
//	uint8_t gpio_index = 1;
	uint8_t temp[LTC6804_REGISTER_GROUP_SIZE + LTC6804_PEC_SIZE];
//	uint8_t byte;
	//uint8_t gpio_select = 2; //Select GPIO port 5.
	
	
	address = LTC6804_GET_BOARD_ADDRESS(pack_num);
	/*Wakeup communication between master and slave*/
	wakeup();	
	
	command_code = LTC6804_COMMANDCODE_RDAUXA(address);
	
	// Send the command code
	ltc6804_send_commandcode(command_code, TRUE);
	
	/*Check if the slave reading are valid and store the data*/
	if(ltc6804_register_group_read(temp) == TRUE)
	{
		*adc_value_ptr = ((uint16_t)temp[3] << 8) + (uint16_t)temp[2];
	}
	else
	{
		/*Save Error message for Reading the GPIO pin*/
		success = FALSE;
	}
	return success;
}

/*Clear voltage register groups*/
void LTC6804_Voltage_Clear(uint8_t pack_num)
{
	uint8_t address;
	uint16_t command_code;
	
	address = LTC6804_GET_BOARD_ADDRESS(pack_num);
	/*Wakeup communication between master and slave*/
	wakeup();
	
	/*create the clear AUX command code*/
	command_code = LTC6804_COMMANDCODE_CLRCELL(address);
	
	ltc6804_send_commandcode(command_code, FALSE);
	
}

/*Start Conversion of voltage readings*/
void LTC6804_Voltage_Start(uint8_t pack_num, BOOLEAN discharge_permitted)
{
	uint8_t address;
	uint16_t command_code;
	uint8_t md = 0x2;		//Normal Reading mode
	uint8_t cell_select = 0x0;		//select all cells
	
	address = LTC6804_GET_BOARD_ADDRESS(pack_num);
	/*Wakeup communication between master and slave*/
	wakeup();
	
	command_code = LTC6804_COMMANDCODE_ADCV(address, md, (discharge_permitted ? LTC6804_DCP_DISCHARGE_PERMITTED : LTC6804_DCP_DISCHARGE_NOTPERMITTED), cell_select);
	
	ltc6804_send_commandcode(command_code, FALSE);
	
}
/*Read ADC voltage register from Slave*/
BOOLEAN LTC6804_Voltage_Read(uint8_t pack_num, uint16_t *adc_value_ptr)
{
	BOOLEAN success = TRUE;
	uint8_t address;
	uint16_t command_code;
//	uint8_t reg_count;
//	uint8_t reg_index = 0x0;		//Channel All cells
	uint8_t reg_inc = 0x2;			//Increase register count by two
//	uint8_t byte_index;
	uint8_t temp[LTC6804_REGISTER_GROUP_SIZE + LTC6804_PEC_SIZE];
	uint8_t byte_num;
//	uint8_t cell_select = 0x0;

	
	address = LTC6804_GET_BOARD_ADDRESS(pack_num);
	/*Wakeup communication between master and slave*/
	wakeup();
	
	command_code = LTC6804_COMMANDCODE_RDCVA(address);
	
	for(uint8_t i = 0; i < LTC6804_RDGROUP_SIZE;i++)
	{		
		// Send the command code
		ltc6804_send_commandcode(command_code, TRUE);
		delay_us(10);
		/*Check if the slave reading are valid and store the data*/
		if(ltc6804_register_group_read(temp) == TRUE)
		{
			for(byte_num = 0; byte_num < LTC6804_REGISTER_GROUP_SIZE; byte_num += 2)			
			{
				*adc_value_ptr++ = ((uint16_t)temp[byte_num + 1] << 8) + (uint16_t)temp[byte_num];
			}
//			/*increase the register wich are needed to be read.*/
//			command_code += reg_inc;	
		}
		else
		{
			/*Save Error message for Reading the GPIO pin*/
			//success = TRUE;
			success = FALSE;
		}	
		/*increase the register wich are needed to be read.*/
		command_code += reg_inc;	
	}
	return success;	
}

/*Write on I2C for LTC1380 communication*/
void LTC6804_I2C_Write(uint8_t pack_num, uint8_t sensor, BOOLEAN enable)
{
	uint8_t address;
	uint16_t command_code;
	uint8_t comm[LTC6804_REGISTER_GROUP_SIZE];
	uint8_t *comm_ptr;
	//uint8_t bytes_to_send;
	//uint8_t byte_num;
	uint8_t msg1;
	uint8_t msg2;
	
	/*Build message for I2C communication*/
	msg1 = LTC1380_COMMANDCODE_SETAD(sensor, TRUE);
	msg2 = LTC1380_COMMANDCODE_SETMUXAD(sensor, enable);
	
	address = LTC6804_GET_BOARD_ADDRESS(pack_num);
	
	/*Wakeup communication between master and slave*/
	wakeup();
	
	command_code = LTC6804_COMMANDCODE_WRCOMM(address);
	ltc6804_send_commandcode(command_code, TRUE);
	
	//bytes_to_send = 0;
	comm_ptr = comm;
	
	*comm_ptr++ =  (LTC6804_ICOM_I2C_WRITE_START << 4) + UPPER_NIBBLE(msg1);
	*comm_ptr++ =  (LOWER_NIBBLE(msg1)  << 4) + LTC6804_FCOM_I2C_WRITE_MASTER_NACK;
	
	*comm_ptr++ =  (LTC6804_ICOM_I2C_WRITE_BLANK << 4) + UPPER_NIBBLE(msg2);
	*comm_ptr++ =  (LOWER_NIBBLE(msg2)  << 4) + LTC6804_FCOM_I2C_WRITE_MASTER_NACKSTOPP;	

	*comm_ptr++ =  (LTC6804_ICOM_I2C_WRITE_NOTRANSMIT << 4) + UPPER_NIBBLE(0XFF);
	*comm_ptr++ =  (LOWER_NIBBLE(0XFF)  << 4) + LTC6804_FCOM_I2C_WRITE_MASTER_NACKSTOPP;
	
	// Write COMM Register Group
  ltc6804_register_group_write(comm);
	delay_us(10);
	/*Construct command code for activate the I2C communication.*/
	command_code = LTC6804_COMMANDCODE_STCOMM(address);
	ltc6804_send_commandcode(command_code, TRUE);
	
	/*Send clock cycles for I2C communication.*/
	for(uint8_t i = 0; i < 9;i++)
	{
		SPI_Write(SPI2,0xFF);
	}
	
	delay_us(30);
	ChipSelect(SPI2, PIN_HIGH);
}

/*---Local Function----------------------------------------------------------------*/
/*Wakeup the communication for the LTC6804*/
void wakeup (void)
{
	uint32_t wakeup_timestampNew;
	
	/*Check timestamp to evaluate if short or long delay is nessesary*/
	wakeup_timestampNew = xTaskGetTickCount();
	
	// enable SPI to wake up Slave 
	ChipSelect(SPI2, PIN_LOW);	
	delay_us(1);
	ChipSelect(SPI2, PIN_HIGH);
		
	if((wakeup_timestampNew - ltc6804_wakeup_timestamp) < LTC6804_TSLEEP)
	{
		// Short delay, if time since last wakeupcall was less than Tsleep ago.
		delay_us(LTC6804_TREADY);
	}
	else 
	{
		delay_us(LTC6804_TWAKE);
	}
	ltc6804_wakeup_timestamp = wakeup_timestampNew;
}

/*Send command to LTC6804*/
void ltc6804_send_commandcode(uint16_t command_code, BOOLEAN reg_group_command)
{
	uint8_t writebyte[LTC6804_COMMAND_SIZE + LTC6804_PEC_SIZE];
	uint16_t pec;
	
	ChipSelect(SPI2, PIN_LOW);
	
	writebyte[0] = UPPER_BYTE(command_code);
	writebyte[1] = LOWER_BYTE(command_code);
	
	/*Send command followed by PEC on SPI*/
	SPI_Write_Array(SPI2, writebyte, LTC6804_COMMAND_SIZE);
	pec = LTC6804_PEC_Calc(writebyte, LTC6804_COMMAND_SIZE);
	writebyte[2] = UPPER_BYTE(pec);
	writebyte[3] = LOWER_BYTE(pec);	
	SPI_Write_Array(SPI2, &writebyte[LTC6804_COMMAND_SIZE], LTC6804_PEC_SIZE);
	
	delay_us(30);
	if (reg_group_command == FALSE)
		{
				delay_us(10);
				ChipSelect(SPI2, PIN_HIGH);
		}
}


void ltc6804_cfgr_modify(uint8_t pack_num, uint8_t *register_mask_ptr, uint8_t *register_value_ptr)
{
	uint8_t address;
	uint16_t command_code;
	uint8_t cfgr[LTC6804_REGISTER_GROUP_SIZE + LTC6804_PEC_SIZE];
	uint8_t byte_num;
	
	address = LTC6804_GET_BOARD_ADDRESS(pack_num);
	/*Wakeup communication between master and slave*/
	wakeup();
	
	command_code = LTC6804_COMMANDCODE_RDCFG(address);
	ltc6804_send_commandcode(command_code, TRUE);
	
	if(ltc6804_register_group_read(cfgr) == TRUE)
	{
		for(byte_num = 0; byte_num < LTC6804_REGISTER_GROUP_SIZE; byte_num++)
		{
			cfgr[byte_num] &= ~(*(register_mask_ptr + byte_num));
			cfgr[byte_num] &= ~(*(register_value_ptr + byte_num));
		}
		
		command_code = LTC6804_COMMANDCODE_WRCFG(address);
		ltc6804_send_commandcode(command_code, TRUE);
		
		ltc6804_register_group_write(cfgr);
	}else
	{
		/*Error Message CFGR dodn't quite success*/
	}
	
}

/*Read data from the LTC6804 register*/
BOOLEAN ltc6804_register_group_read(uint8_t *register_group)
{
	uint8_t byte_num;
	uint16_t pec_calc;
	
	SPI_Read(SPI2);
	
	/*Recieve data from slave*/
	for(uint8_t i = 0;i<(LTC6804_REGISTER_GROUP_SIZE + LTC6804_PEC_SIZE);i++)
	{
		register_group[i] = SPI_WriteRead(SPI2, 0xFF);
	}
	
	pec_calc = pecSeedValue;
	
	/*Calculate PEC value*/
	for(byte_num = 0; byte_num < LTC6804_REGISTER_GROUP_SIZE; byte_num++)
	{
		pec_calc = ltc6804_pec_lookup(register_group[byte_num], pec_calc);
	}
	
	pec_calc <<= 1;
	
	// End the communication
	ChipSelect(SPI2, PIN_HIGH);
	delay_us(10);
	if(UPPER_BYTE(pec_calc) != register_group[LTC6804_REGISTER_GROUP_SIZE] ||
		LOWER_BYTE(pec_calc) != register_group[LTC6804_REGISTER_GROUP_SIZE + 1])
	{
		//return TRUE;
		return FALSE;
	}
	return TRUE;
}

void ltc6804_register_group_write(uint8_t *register_group)
{
	uint8_t writebyte[LTC6804_PEC_SIZE];
	uint16_t pec;
	
	/*Send register group and it's PEC code*/
	SPI_Write_Array(SPI2, register_group, LTC6804_REGISTER_GROUP_SIZE);
	pec = LTC6804_PEC_Calc(register_group, LTC6804_REGISTER_GROUP_SIZE);
	writebyte[0] = UPPER_BYTE(pec);
	writebyte[1] = LOWER_BYTE(pec);	
	SPI_Write_Array(SPI2, writebyte, LTC6804_PEC_SIZE);
	
	delay_us(30);
	ChipSelect(SPI2, PIN_HIGH);
}
//! Calculates the LTC6804 CRC over a string of bytes as per datasheet figure 22.
uint16_t LTC6804_PEC_Calc(uint8_t *data, uint8_t length)
{
    uint16_t remainder;

    remainder = pecSeedValue;

    for (uint8_t i = 0; i < length; i++)
    {
        remainder = ltc6804_pec_lookup(data[i], remainder);
    }

    return (remainder * 2); //The CRC15 has a 0 in the LSB so the remainder must be multiplied by 2
}


// calculates the pec for one byte, and returns the intermediate calculation
static uint16_t ltc6804_pec_lookup(uint8_t data, uint16_t remainder)
{
    uint8_t addr;

    addr = ((remainder >> 7) ^ data) & 0xFF;    //calculate PEC table address
    remainder = (remainder << 8) ^ pecTable[addr];             //get value from CRC15Table;

    return remainder;
}
