

/*---Dependencies----------------------------------------------------------------*/
#include "Delay.h"

void delay_cnt(uint8_t cnt);

/*---Local Driver properties----------------------------------------------------------------*/

/*---Public Function prototypes----------------------------------------------------------------*/
void delay_us(uint16_t us)
{
	volatile uint32_t delayus;
	for(delayus = 0; delayus < us;delayus++)
	{
		delay_cnt(14);
	}
}

/*---Local Function prototypes----------------------------------------------------------------*/
void delay_cnt(uint8_t cnt)
{
	volatile uint32_t delaycnt;
	for(delaycnt = 0; delaycnt < cnt; delaycnt++);	
}
