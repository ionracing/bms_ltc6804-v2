#include "BMS_CAN.h"
/* 
* ECU board
* PB5 : CAN2 RX
* PB6 : CAN2 TX
*/
GPIO_InitTypeDef  GPIO_InitStructure;
CAN_InitTypeDef CAN_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;
CAN_FilterInitTypeDef CAN_FilterInitStructure;

void BSP_CAN_init(void){ // CAN Setup

    /* Enable the clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph(GPIO_CAN1_TX), ENABLE);//RCC_AHB1Periph(GPIO_CAN1_TX), ENABLE);
 	
	GPIO_InitStructure.GPIO_Pin = GPIO_PIN_NUM(PIN_CAN1_TX) | GPIO_PIN_NUM(PIN_CAN1_RX);//GPIO_PIN_NUM(PIN_CAN1_RX) | GPIO_PIN_NUM(PIN_CAN1_TX);  
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIO_CAN1_TX, &GPIO_InitStructure);
	
	//Alternate Function for the GPIOB pins 8 & 9.
		
	GPIO_PinAFConfig(GPIO_CAN1_RX, GPIO_PINSOURCE_NUM(PIN_CAN1_RX), GPIO_AF_CAN1);
	GPIO_PinAFConfig(GPIO_CAN1_TX, GPIO_PINSOURCE_NUM(PIN_CAN1_TX), GPIO_AF_CAN1);
	
	//CAN1 enable setup
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);

	/* CAN1 register init */
	CAN_DeInit(CAN1);

	/* CAN1 cell init */
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_3tq;                    

	/* CAN1 Baudrate = 500 kBps (CAN1 clocked at 40 MHz)*/        
	CAN_InitStructure.CAN_BS1 = CAN_BS1_11tq;                    
	CAN_InitStructure.CAN_BS2 = CAN_BS2_2tq;                   
	CAN_InitStructure.CAN_Prescaler = 6 ;                  
	CAN_Init(CAN1, &CAN_InitStructure);

	/* CAN1 reset */
	CAN_FilterInitStructure.CAN_FilterNumber = 0;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Enable FIFO 0 message pending Interrupt */
	CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);
    
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

void BSP_CAN2_init(void){ // CAN Setup
	/* Have to set the can tranceiver EN pin high to enable it */

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6;  //CAN2 RX: PB5,     CAN2 TX: PB6
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	//Alternate Function for the GPIOB pins 5 & 6. 
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_CAN2);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_CAN2);
	
	//CAN2 enable setup
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN2, ENABLE);
    
	/* CAN2 reset */
	CAN_DeInit(CAN2);

	/* CAN2 cell init */
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;                 

	/* CAN2 Baudrate = 500 kBps (CAN1 clocked at 40 MHz)*/        
	CAN_InitStructure.CAN_BS1 = CAN_BS1_11tq;                   
	CAN_InitStructure.CAN_BS2 = CAN_BS2_2tq;                  
	CAN_InitStructure.CAN_Prescaler = 6;                  
	CAN_Init(CAN2, &CAN_InitStructure);

	/* CAN2 filter init */
	CAN_FilterInitStructure.CAN_FilterNumber = 14; //filter number for CAN1 must be 0..13    and filter number for CAN2 must be 14..27
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Enable FIFO 0 message pending Interrupt */
	CAN_ITConfig(CAN2, CAN_IT_FMP0, ENABLE);
	
	NVIC_InitStructure.NVIC_IRQChannel = CAN2_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x2;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

void CANBuffer_Init(BMS_CAN_frame_t *buffer)
{
	for(uint8_t i = 0; i < CAN_BUFFER_SIZE; i++)
	{
		buffer[i].FullFrame.ID = (CAN_BASE_ID + i);
	}
}

void CANBuffer_copy(BMS_CAN_frame_t *buffer, uint16_t *Data, uint8_t cnt, uint8_t base, uint16_t div)
{
	buffer[base+3*cnt].CanStructDouble.d0 = Data[0]/div;
	buffer[base+3*cnt].CanStructDouble.d2 = Data[1]/div;
	buffer[base+3*cnt].CanStructDouble.d4 = Data[2]/div;
	buffer[base+3*cnt].CanStructDouble.d6 = Data[3]/div;
	
	buffer[base+1+3*cnt].CanStructDouble.d0 = Data[4]/div;
	buffer[base+1+3*cnt].CanStructDouble.d2 = Data[5]/div;
	buffer[base+1+3*cnt].CanStructDouble.d4 = Data[6]/div;
	buffer[base+1+3*cnt].CanStructDouble.d6 = Data[7]/div;
	
	buffer[base+2+3*cnt].CanStructDouble.d0 = Data[8]/div;
	buffer[base+2+3*cnt].CanStructDouble.d2 = Data[9]/div;
	buffer[base+2+3*cnt].CanStructDouble.d4 = Data[10]/div;
	buffer[base+2+3*cnt].CanStructDouble.d6 = Data[11]/div																		;
	
}
