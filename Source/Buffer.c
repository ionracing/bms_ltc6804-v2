/*---Dependencies----------------------------------------------------------------*/
#include "buffer.h"

/*---Local Driver properties----------------------------------------------------------------*/

/*---Local function prototypes----------------------------------------------------------------*/
uint16_t buffer_LengthData(circBuffer_t *cb);

/*---Public Function ----------------------------------------------------------------*/
void buffer_init(circBuffer_t* cb, tElement* buffer, uint16_t length)
{
    cb->buf = buffer;
    cb->size = length;
    cb->write = 0;
    cb->read = 0;
}
/*Initalize local buffer for BMS master, */
void bufferData_Init(CanbusWriteBuffer_t *cwb, uint16_t length)
{
	cwb[0].ID = 1;
	/*Give the System name for Canbus messages.*/
	for( uint8_t i = 0; i < 8;i++)
	{
		cwb[0].Data[i] = SYSTEM_NAME[i];
		cwb[1].Data[i] = SYSTEM_REV[i];
	}
	/*Initiate ID and activate write.*/
	for(uint16_t i = 1; i < length; i++)
	{
		cwb[i].ID = i + CAN_BASE_ID;
		cwb[i].read = 0;
		cwb[i].write = 1;
	}
}

enum eError buffer_write(circBuffer_t *cb, tElement data)
{
    if(buffer_LengthData(cb) == (cb->size-1)){return eErrorBufferFull;}
    cb->buf[cb->write] = data;
    cb->write = (cb->write + 1) & (cb->size -1); /* Must be atomic */
    return eErrorNone;
}

enum eError buffer_read(circBuffer_t *cb, tElement *data)
{
    if (buffer_LengthData(cb) == 0){return eErrorBufferEmpty;}
    *data = cb->buf[cb->read];
    cb->read = (cb->read + 1) & (cb->size -1);
    return eErrorNone;
}

/*---Local Function ----------------------------------------------------------------*/
uint16_t buffer_LengthData(circBuffer_t *cb)
{
    return ((cb->write - cb->read) & (cb->size -1));
}
